//
//  ViewController.swift
//  EdurekaModule7c
//
//  Created by Jay on 23/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

//UICollisionBehaviorDelegate will allow you to listen to collision events.
//so we can respond to it.

class ViewController: UIViewController,UICollisionBehaviorDelegate
{
    
    //lets get an animator that will animate movement.
    
    //lets get a gravity element that will add gravity
    
    var animator: UIDynamicAnimator!
    var gravity: UIGravityBehavior!
    
    //lets build some kind of bounday/stopping object
    var collision: UICollisionBehavior!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("viewDidLoad")
        //lets create and then add an object to the view
        
        let square = UIView(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
        square.backgroundColor = UIColor.red
        view.addSubview(square)
        
        //lets add a bar
        let barrier = UIView(frame: CGRect(x: 0, y: 300, width: 130, height: 20))
        barrier.backgroundColor = UIColor.blue
        view.addSubview(barrier)
        
        
        //now lets apply this to the square above.
        //there is no boundary so it will fall off
        animator = UIDynamicAnimator(referenceView: view)
        gravity = UIGravityBehavior(items: [square])
        animator.addBehavior(gravity)
        
        //thanks to this, a boundary is created
        //collision = UICollisionBehavior(items: [square])
        collision = UICollisionBehavior(items: [square, barrier])
        collision.collisionDelegate = self
        //we are setting the screen itself as the boundary here
        collision.translatesReferenceBoundsIntoBoundary = true
        animator.addBehavior(collision)
        
        //okay, lets display whenever something happens to the square.
        collision.action =
        {
            print("\(NSStringFromCGAffineTransform(square.transform)) \(NSStringFromCGPoint(square.center))")
        }
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //lets listen to the event when two items collide.
    
    func collisionBehavior(_ behavior: UICollisionBehavior,
                                    beganContactFor item1: UIDynamicItem,
                                    with item2: UIDynamicItem,
                                    at p: CGPoint)
    {
        print("Begin Collision item and item" + item1.description + item2.description)
    }


}

